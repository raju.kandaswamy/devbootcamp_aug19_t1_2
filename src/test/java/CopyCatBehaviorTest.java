import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class CopyCatBehaviorTest {

    @Test
    public void shouldReturnCooperateFirstTime() {
        CopyCatBehavior copyCatBehavior = new CopyCatBehavior();
        Assert.assertEquals(Move.Cooperate, copyCatBehavior.move());
    }

    @Test
    public void shouldUpdateMoveWhenUpdateCalled() {
        CopyCatBehavior copyCatBehavior = new CopyCatBehavior();
        copyCatBehavior.update(null, Move.Cheat);
        assertEquals(Move.Cheat, copyCatBehavior.move());

    }

}