import org.junit.Assert;
import org.junit.Test;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

import static org.mockito.Mockito.*;

public class EvolutionGameTest {

    private PrintStream printStream = mock(PrintStream.class);

    @Test
    public void shouldReturnInstanceOfGame(){
        EvolutionGame evolutionGame = new EvolutionGame(5, new Player(new ConsoleBehavior(()->1)), new Player(new ConsoleBehavior(()->0)), printStream);
    }

    @Test
    public void gameShouldHave2Players() {
        EvolutionGame evolutionGame = new EvolutionGame(5,new Player(new ConsoleBehavior(()->1)), new Player(new ConsoleBehavior(()->0)), printStream);
        Assert.assertNotNull(evolutionGame.getPlayer1());
        Assert.assertNotNull(evolutionGame.getPlayer2());
    }

    @Test
    public void shouldHaveNumberOfRoundsEqualTo5(){
        EvolutionGame evolutionGame = new EvolutionGame(5,new Player(new ConsoleBehavior(()->1)), new Player(new ConsoleBehavior(()->0)), printStream);
        Assert.assertEquals(5,evolutionGame.getNumberOfRounds());
    }

    @Test
    public void shouldSetNumberOfRoundsTo10(){
        EvolutionGame evolutionGame = new EvolutionGame(10,new Player(new ConsoleBehavior(()->1)), new Player(new ConsoleBehavior(()->0)), printStream);
        Assert.assertEquals(10,evolutionGame.getNumberOfRounds());
    }

    @Test
    public void gameShouldPlayAndUpdatePlayerScore(){
        EvolutionGame evolutionGame = new EvolutionGame(1,new Player(new ConsoleBehavior(()->1)), new Player(new ConsoleBehavior(()->1)), printStream);
        evolutionGame.play();

        Assert.assertEquals(2, evolutionGame.getPlayer1().getScore());
        Assert.assertEquals(2, evolutionGame.getPlayer2().getScore());

        evolutionGame = new EvolutionGame(2,new Player(new ConsoleBehavior(new GameInput(new Scanner("1\n0")))), new Player(new ConsoleBehavior(new GameInput(new Scanner("1\n1")))), printStream);
        evolutionGame.play();

        Assert.assertEquals(5, evolutionGame.getPlayer1().getScore());
        Assert.assertEquals(1, evolutionGame.getPlayer2().getScore());

        evolutionGame = new EvolutionGame(5,new Player(new CheaterBehavior()), new Player(new CooperatorBehavior()), printStream);
        evolutionGame.play();

        Assert.assertEquals(15, evolutionGame.getPlayer1().getScore());
        Assert.assertEquals(-5, evolutionGame.getPlayer2().getScore());
    }

    @Test
    public void shouldPlayAndRetunZeroForBothPlayers() {
        Player player1 = mock(Player.class);
        Player player2 = mock(Player.class);

        when(player1.move()).thenReturn(Move.Cooperate).thenReturn(Move.Cooperate);
        when(player2.move()).thenReturn(Move.Cheat).thenReturn(Move.Cooperate);

        EvolutionGame evolutionGame = new EvolutionGame(2, player1, player2, printStream);

        evolutionGame.play();

        verify(player1).setScore(-1);
        verify(player2).setScore(3);
        verify(player1).setScore(2);
        verify(player2).setScore(2);
    }

    @Test
    public void afterEachRoundNotifierPlayersIsCalled() {
        Player player1 = mock(Player.class);
        CopyCatBehavior copyCatBehavior = mock(CopyCatBehavior.class);
        Player copycatPlayer = new Player(copyCatBehavior);

        when(player1.move()).thenReturn(Move.Cheat).thenReturn(Move.Cheat);
        when(copyCatBehavior.move()).thenReturn(Move.Cooperate).thenReturn(Move.Cheat);

        EvolutionGame evolutionGame = new EvolutionGame(2, player1, copycatPlayer, printStream);
        evolutionGame.addObserver(copyCatBehavior);
        evolutionGame.play();

        ArrayList<Move> moves = new ArrayList<Move>(Arrays.asList(Move.Cheat, Move.Cooperate));
        verify(copyCatBehavior, times(1)).update(evolutionGame, moves);
    }

    @Test
    public void shouldPlayGameBetweenCopyCatAndAlwaysCheat() {
        Player cheatPlayer = new Player(new CheaterBehavior());
        CopyCatBehavior copyCatBehavior = new CopyCatBehavior();
        Player copycatPlayer = new Player(copyCatBehavior);
        EvolutionGame evolutionGame = new EvolutionGame(5, cheatPlayer, copycatPlayer, printStream);

        evolutionGame.addObserver(copyCatBehavior);
        evolutionGame.play();

        Assert.assertEquals(3, evolutionGame.getPlayer1().getScore());
        Assert.assertEquals(-1, evolutionGame.getPlayer2().getScore());

    }

    @Test
    public void shouldPlayGameBetweenCopyCatAndGrudger() {
        GrudgerBehavior grudgerBehavior = new GrudgerBehavior();
        Player grudgerPlayer = new Player(grudgerBehavior);
        CopyCatBehavior copyCatBehavior = new CopyCatBehavior();
        Player copycatPlayer = new Player(copyCatBehavior);
        EvolutionGame evolutionGame = new EvolutionGame(5, grudgerPlayer, copycatPlayer, printStream);

        evolutionGame.addObserver(grudgerBehavior);
        evolutionGame.addObserver(copyCatBehavior);
        evolutionGame.play();

        verify(printStream).println("player 1:2, player 2:2");
        verify(printStream).println("player 1:4, player 2:4");
        verify(printStream).println("player 1:6, player 2:6");
        verify(printStream).println("player 1:8, player 2:8");
        verify(printStream).println("player 1:10, player 2:10");
    }

    @Test
    public void shouldPlayGameBetweenCopyCatandCopyCat() {

        CopyCatBehavior copyCatBehavior1 = new CopyCatBehavior();
        Player copycatPlayer = new Player(copyCatBehavior1);
        CopyCatBehavior copyCatBehavior2 = new CopyCatBehavior();
        Player copycatPlayer2 = new Player(copyCatBehavior1);
        EvolutionGame evolutionGame = new EvolutionGame(5, copycatPlayer, copycatPlayer2, printStream);

        evolutionGame.addObserver(copyCatBehavior1);
        evolutionGame.addObserver(copyCatBehavior2);

        evolutionGame.play();

        verify(printStream).println("player 1:2, player 2:2");
        verify(printStream).println("player 1:4, player 2:4");
        verify(printStream).println("player 1:6, player 2:6");
        verify(printStream).println("player 1:8, player 2:8");
        verify(printStream).println("player 1:10, player 2:10");
    }
}
