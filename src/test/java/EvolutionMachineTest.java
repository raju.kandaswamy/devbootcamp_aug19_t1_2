import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EvolutionMachineTest {

    private EvolutionMachine machine;

    @Before
    public void setup()
    {
        machine = new EvolutionMachine();
    }

    @Test
    public void shouldReturn2ScoreForEachWhenBothCooperating()
    {

        int[] scores = machine.calculateScore(Move.Cooperate,Move.Cooperate);
        Assert.assertEquals(2,scores[0]);
        Assert.assertEquals(2,scores[1]);
    }

    @Test
    public void shouldReturn3and_1WhenPlayer1CheatsPlayer2Cooperates()
    {
        int[] scores = machine.calculateScore(Move.Cheat,Move.Cooperate);
        Assert.assertEquals(3,scores[0]);
        Assert.assertEquals(-1,scores[1]);
    }

    @Test
    public void shouldReturn_1and3WhenPlayer1CooperatesPlayer2Cheats()
    {
        int[] scores = machine.calculateScore(Move.Cooperate,Move.Cheat);
        Assert.assertEquals(-1,scores[0]);
        Assert.assertEquals(3,scores[1]);
    }
    @Test
    public void shouldReturnZeroWhenBothCheats() {
        int[] scores = machine.calculateScore(Move.Cheat, Move.Cheat);
        Assert.assertEquals(0, scores[0]);
        Assert.assertEquals(0, scores[1]);
    }
}
