import org.junit.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class GrudgerBehaviorTest {

    private GrudgerBehavior grudgerBehavior = new GrudgerBehavior();

    @Test
    public void shouldReturnGrudgerBehaviourInstance() {
        assertNotNull(grudgerBehavior);
    }

    @Test
    public void grudgerShouldCooperateInitially() {
        assertEquals(Move.Cooperate, grudgerBehavior.move());
    }

    @Test
    public void grudgerShouldReturnCheatMoveAfterBeingCheated() {
        grudgerBehavior.update(null, Arrays.asList(Move.Cooperate, Move.Cheat));
        assertEquals(Move.Cheat, grudgerBehavior.move());
    }

}
