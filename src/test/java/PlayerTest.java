import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PlayerTest {


    @Test
    public void shouldReturnCorrectNextMove()
    {

        Move nextMove = new Player(new ConsoleBehavior(()->1)).move();
        Assert.assertEquals(Move.Cooperate, nextMove);

        nextMove = new Player(new ConsoleBehavior(()->0)).move();
        Assert.assertEquals(Move.Cheat, nextMove);

        nextMove = new Player(new CheaterBehavior()).move();
        Assert.assertEquals(Move.Cheat, nextMove);

        nextMove = new Player(new CooperatorBehavior()).move();
        Assert.assertEquals(Move.Cooperate, nextMove);
    }

    @Test
    public void shouldSetAndGetScore(){
        Player player = new Player(new ConsoleBehavior(()->0));
        player.setScore(2);
        Assert.assertEquals(2,player.getScore());
    }


    @Test
    public void shouldReturnCoOperateAsFirstMoveForCopyCat() {
        Player copyCatPlayer = new Player(new CopyCatBehavior());
        assertEquals(Move.Cooperate, copyCatPlayer.move());
    }

}
