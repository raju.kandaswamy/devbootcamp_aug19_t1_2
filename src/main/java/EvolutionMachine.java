public class EvolutionMachine {

    public static final int GOODCOOPERATIONSCORE = 2;
    public static final int OPPONENTCHEATINGSCORE = -1;
    public static final int CHEATSCORE = 3;

    public int[] calculateScore(Move firstPlayer, Move secondPlayer)
    {
        int[] scores = new int[2];

        if(firstPlayer.equals(Move.Cooperate)&&secondPlayer.equals(Move.Cooperate)){
            scores[0] = GOODCOOPERATIONSCORE;
            scores[1] = GOODCOOPERATIONSCORE;
        }
        else if(firstPlayer.equals(Move.Cooperate) && secondPlayer.equals(Move.Cheat)){
            scores[0] = OPPONENTCHEATINGSCORE;
            scores[1] = CHEATSCORE;
        }
        else if(firstPlayer.equals(Move.Cheat) && secondPlayer.equals(Move.Cooperate)){
            scores[0] = CHEATSCORE;
            scores[1] = OPPONENTCHEATINGSCORE;
        }
        return scores;
    }
}
