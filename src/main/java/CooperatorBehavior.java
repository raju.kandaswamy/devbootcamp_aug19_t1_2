public class CooperatorBehavior implements PlayerBehavior {

    @Override
    public Move move() {
        return Move.Cooperate;
    }
}