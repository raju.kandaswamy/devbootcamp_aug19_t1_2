public class ConsoleBehavior implements PlayerBehavior {
    IGameInput scanner;

    public ConsoleBehavior(IGameInput scanner) {
        this.scanner = scanner;
    }

    @Override
    public Move move() {
        if (this.scanner.nextInput() == 1) {
            return Move.Cooperate;
        } else {
            return Move.Cheat;
        }
    }
}