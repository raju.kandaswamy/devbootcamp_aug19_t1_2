import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;


public class CopyCatBehavior implements PlayerBehavior, Observer {

    private Move move;

    @Override
    public Move move() {
        return move == null ? Move.Cooperate : move;
    }

    @Override
    public void update(Observable o, Object arg) {
        ArrayList<Move> moves = (ArrayList<Move>) arg;
        Move move1 = moves.get(0);
        Move move2 = moves.get(1);
        if (move != move1) {
            move = move1;
        } else {
            move = move2;
        }
    }
}
