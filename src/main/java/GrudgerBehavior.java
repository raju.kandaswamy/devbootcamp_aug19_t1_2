import java.util.List;
import java.util.Observable;
import java.util.Observer;

public class GrudgerBehavior implements PlayerBehavior, Observer {

    private Move move = Move.Cooperate;

    @Override
    public Move move() {
        return move;
    }

    @Override
    public void update(Observable o, Object arg) {
        List<Move> moves = (List<Move>) arg;
        if (moves.contains(Move.Cheat)) {
            move = Move.Cheat;
        }
    }
}
