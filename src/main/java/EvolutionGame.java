import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Scanner;

public class EvolutionGame extends Observable {

    private int numberOfRounds;
    private Player player1;
    private Player player2;
    private PrintStream printStream;
    private EvolutionMachine machine;


    public EvolutionGame(int numberOfRounds, Player player1, Player player2, PrintStream printStream){
        this.numberOfRounds = numberOfRounds;
        this.player1 = player1;
        this.player2 = player2;
        this.printStream = printStream;
        machine = new EvolutionMachine();
    }
    public Player getPlayer2() {

        return player2;
    }

    public Player getPlayer1() {
        return player1;
    }

    public int getNumberOfRounds() {
        return this.numberOfRounds;
    }

    public void play() {

        while(numberOfRounds>0) {
            System.out.println("Enter Player 1 Move:");
            Move player1Move = player1.move();
            System.out.println("Enter Player 2 Move:");
            Move player2Move = player2.move();
            int[] scores = machine.calculateScore(player1Move, player2Move);
            player1.setScore(player1.getScore() + scores[0]);
            player2.setScore(player2.getScore() + scores[1]);
            numberOfRounds--;

            printStream.println("player 1:"+ player1.getScore() + ", player 2:" + player2.getScore());

            ArrayList<Move> moves = new ArrayList<Move>();
            moves.add(player1Move);
            moves.add(player2Move);

            setChanged();
            notifyObservers(moves);
        }
    }



    public static void main(String[] args)
    {
        System.out.println("Enter number of Rounds:");
        EvolutionGame game = new EvolutionGame(new Scanner(System.in).nextInt(), new Player(new ConsoleBehavior(new GameInput(new Scanner(System.in)))),new Player(new ConsoleBehavior(new GameInput(new Scanner(System.in)))), System.out);
        game.play();
        System.out.println("Final Score for Player 1 is " + game.getPlayer1().getScore());
        System.out.println("Final Score for Player 2 is " + game.getPlayer2().getScore());
    }


}
