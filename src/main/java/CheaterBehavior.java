public class CheaterBehavior implements PlayerBehavior {

    @Override
    public Move move() {
        return Move.Cheat;
    }
}