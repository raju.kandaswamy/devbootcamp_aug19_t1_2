import java.util.Scanner;

public class GameInput implements IGameInput {

    private Scanner scanner;
    public GameInput(Scanner scanner) {
        this.scanner = scanner;
    }

    @Override
    public int nextInput() {
        return scanner.nextInt();
    }
}
