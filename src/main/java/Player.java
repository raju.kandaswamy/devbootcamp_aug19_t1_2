public class Player{

    private PlayerBehavior playerBehavior;
    private int score = 0;

    public Player(PlayerBehavior playerBehavior) {
        this.playerBehavior = playerBehavior;
    }

    public Move move() {
        return playerBehavior == null ? Move.Cooperate : playerBehavior.move();
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int getScore() {
        return this.score;
    }

}
